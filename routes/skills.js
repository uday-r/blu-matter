const express = require('express');
const router = express.Router();

const SkillService = require('../services/skillService')

router.get('/list', async function(req, res, next) {
  const skillName = req.query.name;

  try {
    const skills = await SkillService.searchSkillByName(skillName);
    return res.send(skills)
  } catch (e) {
    console.log(e);
    res.status(500).send("Something went wrong!")
  }

});

module.exports = router;