const express = require('express');
const router = express.Router();

const IndustryService = require('../services/industryService')

router.get('/list', async function(req, res, next) {
  const industryName = req.query.name;

  try {
    const industries = await IndustryService.searchIndustryByName(industryName);
    return res.send(industries)
  } catch(e) {
  	console.log(e);
    return res.status(500).send("Something went wrong!")
  }

});

module.exports = router;