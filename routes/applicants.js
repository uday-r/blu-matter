const express = require('express');
const router = express.Router();

const ApplicantService = require('../services/applicantService')
const Applicant = require('../models/Applicant');

router.get('/', async function(req, res, next) {

  console.log("Applicants listing");
  try {
    const applicants = await ApplicantService.list()
    res.send(applicants);
  } catch (e) {
    console.log(e)
    res.status(400).send("Something went wrong.")
  }
});

router.get('/listByProjectId', async function(req, res, next) {
  let skills = req.query.skills;
  let industry = req.query.industry;

  let projectId = req.query.projectId;
  if(!projectId) {
    return res.status(400).send("No project-id is sent");
  }

  console.log("Applicants listing", projectId);
  const rows = await ApplicantService
    .getApplicantProirityListByProjectId(projectId)
  res.send(rows);
});

router.post('/', async function(req, res, next) {
  const applicantDetails = req.body;
  applicantDetails.industry._id = applicantDetails.industry.id;
  delete applicantDetails.industry.id;
  applicantDetails
    .skills
    .forEach(function(skill) {
      skill._id = skill.id;
      delete skill.id;
    });
  try {
    const applicant = await ApplicantService.create(applicantDetails);
    res.json(applicant);
  } catch(e) {
    console.log(e);
    res.status(400).send("Something went wrong")
  }
})




module.exports = router;