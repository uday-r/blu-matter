var express = require('express');
var router = express.Router();
const ProjectService = require('../services/projectService')

router.get('/', async function(req, res, next) {
  res.json(await ProjectService.list());
});

router.get('/:projectId', async function(req, res, next) {
  const project = await ProjectService.findOne(req.params.projectId);
  if(project) {
  	res.json(project);
  	return;
  }
  res.status(404).send("Project not found")
});

router.post('/', async function(req, res, next) {
  const projectDetails = req.body;
  projectDetails.industry._id = projectDetails.industry.id;
  delete projectDetails.industry.id;
  projectDetails
    .skills
    .forEach(function(skill) {
      skill._id = skill.id;
      delete skill.id;
    });
  try {
    const project = await ProjectService.create(req.body);
    res.json(project);
  } catch(e) {
    console.log(e);
    res.status(400).send("Something went wrong")
  }
})

module.exports = router;