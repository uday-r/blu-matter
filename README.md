# Blu Matter
- POC which suggests applicants based on their skills and CV, [demo](https://blu-matter.herokuapp.com/).


### Technologies used
- Frontend: angularJs, angular-material.
- Server: nodejs, express.
- DB: mongo

### Installation:
- Pretty easy download the code and run `npm install` then next `npm start`.
- It uses an external mongoDB(mLab) so you dont need to run a local instance.
- If you really want to be able to use local instance, openup `./config.js` and change `uri` attribute.


### What does it do?
- It lists `Projects` and `JobApplicants`.
- You can create a project with some meta info and save it.
- Use similar information while creating a job applicant, especially the indusrty.
- For a applicant to show-up a in suggestions they need to belong to same industry.
- All mathcing applicants are listed in decreasing order of their matching criteria.


### What is match criteria?
- Skills in project and skills in applicants are matched using regex and a match percentage is generated
```
	project.skills = ['nodejs', 'angularJs', 'express', 'mongo']
	applicant.skills = ['nodejs', 'angularJs']
	// Here the match criteria is 50%.
	// The match criteria is indicated by a bar which displays 50% on hover.
```

### How are the suggestions sorted?
- Use 3 parameters `experience + skillMatchPercentage + resumeMatches`.
- Applicants's  experience.
- `skillMatchPercentage` generated in previous step.
- `resumeMatches` is a regex match of project-skills on applicants's resume.