angular.module('BluMatter').
  factory('IndustryService', ['$http', function($http) {
    return {
      search: function(name) {
        return $http({
          method: "GET",
          url: "/api/industries/list/",
          params: {
            name: name
          }
        });
      },
    }
  }]);