angular.module('BluMatter').
  factory('SkillService', ['$http', function($http) {
    return {
      search: function(name) {
        return $http({
          method: "GET",
          url: "/api/skills/list",
          params: {
            name: name
          }
        });
      },
    }
  }]);