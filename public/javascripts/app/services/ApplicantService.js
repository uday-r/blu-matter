angular.module('BluMatter').
  factory('ApplicantService', ['$http', function($http) {
    return {
      getMatchingApplicants: function(projectId) {
        return $http({
          method: "GET",
          url: "/api/applicants/listByProjectId",
          params: {
            projectId: projectId
          }
        });
      },
      list: function() {
        return $http({
          method: "GET",
          url: "/api/applicants/",
        });
      },
      create: function(applicantDetails) {
        return $http({
          method: "POST",
          url: "/api/applicants/",
          data: applicantDetails
        }); 
      }
    }
  }]);