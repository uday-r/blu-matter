angular.module('BluMatter').
  factory('ProjectService', ['$http', function($http) {
    return {
      list: function() {
        return $http({
          method: "GET",
          url: "/api/projects/"
        });
      },
      create: function(projectDetails) {
        return $http({
          method: "POST",
          url: "/api/projects/",
          data: projectDetails
        });
      },
    }
  }]);