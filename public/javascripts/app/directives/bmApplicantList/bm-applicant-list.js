angular.module('BluMatter')
.directive('bmApplicantList', function($rootScope, $timeout) {
    return {
      restrict: 'E',
      templateUrl: '/javascripts/app/directives/bmApplicantList/bm-applicant-list.html',
      scope: false,
      controller: [
        '$scope',
        '$mdToast',
        'ApplicantService',
         function(
          $scope,
          $mdToast,
          ApplicantService) {

          ApplicantService
            .list()
            .then(function(res) {
              $scope.applicants = res.data;
            }, function(err) {
              $mdToast.showSimple("Something went wrong");
            })

      }]
  }
});