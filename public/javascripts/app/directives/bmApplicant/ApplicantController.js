angular.module('BluMatter')
  .controller('ApplicantController',[
    '$scope',
    'SkillService',
    'IndustryService',
    'ApplicantService',
    '$mdDialog',
    '$mdToast',
    '$q'
    , function(
      $scope,
      SkillService,
      IndustryService,
      ApplicantService,
      $mdDialog,
      $mdToast,
      $q
    ) {

      $scope.applicant = {
        name:'',
        experience: 0,
        skills: [],
        industry: '',
        description: '',
      }

      $scope.searchSkill = function(skillName) {
        var defer = $q.defer();
        SkillService
          .search(skillName)
          .then(function(res) {
            defer.resolve(res.data);
          }, function(err) {
            console.log("Err: ", err);
          });

        return defer.promise;
      }

      $scope.searchIndustry = function(industryName) {
        var defer = $q.defer();
        IndustryService
          .search(industryName)
          .then(function(res) {
            defer.resolve(res.data);
          }, function(err) {
            console.log("Err: ", err);
          });

        return defer.promise;
      }

      $scope.save = function() {
        ApplicantService
          .create($scope.applicant)
          .then(function(applicantDetails) {
            $mdToast.showSimple('Saved successfully.')
            $mdDialog.hide(applicantDetails.data);
          })
          .catch(function(err) {
            console.log(err);
            $mdToast.showSimple('Could not save, please try again later.')
          })
      }

      $scope.close = function() {
        $mdDialog.cancel();
      }

  }]);
