angular.module('BluMatter')
.directive('bmApplicant', function($rootScope, $timeout) {
    return {
      restrict: 'A',
      scope: false,
      link: function(scope, el, attrs) {
        el.on('click', function() {
          scope.showApplicantPopup();
        })
      },
      controller: [
        '$scope',
        '$mdToast',
        '$mdDialog',
        'ApplicantService',
         function(
          $scope,
          $mdToast,
          $mdDialog,
          ApplicantService) {

          $scope.showApplicantPopup = function() {
            $mdDialog
              .show({
                templateUrl: '/javascripts/app/directives/bmApplicant/bm-applicant.html',
                controller: 'ApplicantController'
              })
              .then(function(newApplicant) {
                $scope.applicants.push(newApplicant);
              })
          }

      }]
  }
});