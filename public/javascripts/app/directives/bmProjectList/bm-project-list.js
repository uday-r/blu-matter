angular.module('BluMatter')
.directive('bmProjectList', function($rootScope, $timeout) {
    return {
      restrict: 'E',
      templateUrl: '/javascripts/app/directives/bmProjectList/bm-project-list.html',
      scope: false,
      controller: [
        '$scope',
        '$mdToast',
        'ProjectService',
         function(
          $scope,
          $mdToast,
          ProjectService) {

          ProjectService
            .list()
            .then(function(res) {
              $scope.projects = res.data;
            }, function(err) {
              $mdToast.showSimple("Something went wrong");
            })

      }]
  }
});