angular.module('BluMatter')
.directive('bmMatchingApplicant', function($rootScope, $timeout) {
    return {
      restrict: 'E',
      templateUrl: '/javascripts/app/directives/bmProjectList/bmProjectItem/bmMatchingApplicant/bm-matching-applicant.html',
      scope: false,
      controller: [
        '$scope',
         function(
          $scope) {

          $scope.expand = function() {
            $scope.applicant.showMore = !$scope.applicant.showMore;
          }

      }]
  }
});