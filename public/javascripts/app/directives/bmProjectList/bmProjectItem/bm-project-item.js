angular.module('BluMatter')
.directive('bmProjectItem', function($rootScope, $timeout) {
    return {
      restrict: 'E',
      templateUrl: '/javascripts/app/directives/bmProjectList/bmProjectItem/bm-project-item.html',
      scope: false,
      controller: [
        '$scope',
        '$mdToast',
        'ApplicantService',
         function(
          $scope,
          $mdToast,
          ApplicantService) {

          $scope.expand = function() {
            $scope.showMore = !$scope.showMore;
          }

          $scope.listMatchingParticipants = function() {
            if($scope.project.matchingApplicants) {
              return;
            }
            $scope.loadingParticipants = true;
            ApplicantService
              .getMatchingApplicants($scope.project.id)
              .then(function(res) {
                $scope.loadingParticipants = false;
                $scope.project.matchingApplicants = res.data;
              })
              .catch(function(err) {
                $scope.loadingParticipants = false;
                $mdToast.showSimple("Could not load matching applicants.")
              });
          }

      }]
  }
});