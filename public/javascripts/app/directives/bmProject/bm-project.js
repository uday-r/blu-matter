angular.module('BluMatter')
.directive('bmProject', function($rootScope, $timeout) {
    return {
      restrict: 'A',
      scope: false,
      link: function(scope, el, attrs) {
        el.on('click', function() {
          scope.showProjectPopup();
        })
      },
      controller: [
        '$scope',
        '$mdToast',
        '$mdDialog',
        'ProjectService',
         function(
          $scope,
          $mdToast,
          $mdDialog,
          ProjectService) {

          $scope.showProjectPopup = function() {
            $mdDialog
              .show({
                templateUrl: '/javascripts/app/directives/bmProject/bm-project.html',
                controller: 'ProjectController'
              })
              .then(function(newProject) {
                $scope.projects.push(newProject);
              })
              .catch(function() {
                console.log("Closed dialog");
              })
          }

      }]
  }
});