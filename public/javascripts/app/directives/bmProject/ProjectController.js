angular.module('BluMatter')
  .controller('ProjectController',[
    '$scope',
    'SkillService',
    'IndustryService',
    'ProjectService',
    '$mdDialog',
    '$mdToast',
    '$q'
    , function(
      $scope,
      SkillService,
      IndustryService,
      ProjectService,
      $mdDialog,
      $mdToast,
      $q
    ) {

      $scope.project = {
        name:'',
        reqExperience: 0,
        skills: [
        ],
        industry: '',
        description: '',
      }

      $scope.searchSkill = function(skillName) {
        var defer = $q.defer();
        SkillService
          .search(skillName)
          .then(function(res) {
            defer.resolve(res.data);
          }, function(err) {
            console.log("Err: ", err);
          });

        return defer.promise;
      }

      $scope.searchIndustry = function(industryName) {
        var defer = $q.defer();
        IndustryService
          .search(industryName)
          .then(function(res) {
            defer.resolve(res.data);
          }, function(err) {
            console.log("Err: ", err);
          });

        return defer.promise;
      }

      $scope.save = function() {
        ProjectService
          .create($scope.project)
          .then(function(projectDetails) {
            $mdToast.showSimple('Saved successfully.')
            $mdDialog.hide(projectDetails.data);
          })
          .catch(function(err) {
            console.log(err);
            $mdToast.showSimple('Could not save, please try again later.')
          })
      }

      $scope.close = function() {
        $mdDialog.cancel();
      }

  }]);
