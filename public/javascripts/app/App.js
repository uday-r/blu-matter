const app = angular.module('BluMatter', [
	'ngRoute',
	'ngCookies',
	'ngAnimate',
	'ngSanitize',
	'ngMessages',
	'ngMaterial'
]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "/routes/projects.html"
    })
    .when("/applicants", {
        templateUrl : "/routes/applicants.html"
    })
});