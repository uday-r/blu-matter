const Skill = require('./models/Skill');
const Industry = require('./models/Industry');

const skillStr = "Accounting|Airlines/Aviation|Alternative Dispute Resolution|Alternative Medicine|Animation|Apparel & Fashion|Architecture & Planning|Arts and Crafts|Automotive|Aviation & Aerospace|Banking|Biotechnology|Broadcast Media|Building Materials|Business Supplies and Equipment|Capital Markets|Chemicals|Civic & Social Organization|Civil Engineering|Commercial Real Estate|Computer & Network Security|Computer Games|Computer Hardware|Computer Networking|Computer Software|Construction|Consumer Electronics|Consumer Goods|Consumer Services|Cosmetics|Dairy|Defense & Space|Design|Education Management|E-Learning|Electrical/Electronic Manufacturing|Entertainment|Environmental Services|Events Services|Executive Office|Facilities Services|Farming|Financial Services|Fine Art|Fishery|Food & Beverages|Food Production|Fund-Raising|Furniture|Gambling & Casinos|Glass, Ceramics & Concrete|Government Administration|Government Relations|Graphic Design|Health, Wellness and Fitness|Higher Education|Hospital & Health Care|Hospitality|Human Resources|Import and Export|Individual & Family Services|Industrial Automation|Information Services|Information Technology and Services|Insurance|International Affairs|International Trade and Development|Internet|Investment Banking|Investment Management|Judiciary|Law Enforcement|Law Practice|Legal Services|Legislative Office|Leisure, Travel & Tourism|Libraries|Logistics and Supply Chain|Luxury Goods & Jewelry|Machinery|Management Consulting|Maritime|Market Research|Marketing and Advertising|Mechanical or Industrial Engineering|Media Production|Medical Devices|Medical Practice|Mental Health Care|Military|Mining & Metals|Motion Pictures and Film|Museums and Institutions|Music|Nanotechnology|Newspapers|Non-Profit Organization Management|Oil & Energy|Online Media|Outsourcing/Offshoring|Package/Freight Delivery|Packaging and Containers|Paper & Forest Products|Performing Arts|Pharmaceuticals|Philanthropy|Photography|Plastics|Political Organization|Primary/Secondary Education|Printing|Professional Training & Coaching|Program Development|Public Policy|Public Relations and Communications|Public Safety|Publishing|Railroad Manufacture|Ranching|Real Estate|Recreational Facilities and Services|Religious Institutions|Renewables & Environment|Research|Restaurants|Retail|Security and Investigations|Semiconductors|Shipbuilding|Sporting Goods|Sports|Staffing and Recruiting|Supermarkets|Telecommunications|Textiles|Think Tanks|Tobacco|Translation and Localization|Transportation/Trucking/Railroad|Utilities|Venture Capital & Private Equity|Veterinary|Warehousing|Wholesale|Wine and Spirits|Wireless|Writing and Editing|"
const industryStr = "None|Accounting|Administrative|Arts and Design|Business Development|Community & Social Services|Consulting|Education|Engineering|Entrepreneurship|Finance|Healthcare Services|Human Resources|Information Technology|Legal|Marketing|Media & Communications|Military & Protective Services|Operations|Product Management|Program & Product Management|Purchasing|Quality Assurance|Real Estate|Rersearch|Sales|Support|"

var checkIfSkillsPopulated = async function() {
	return await Skill.countDocuments() > 0;
}

var populateSkillsAndIndustryMeta = async function() {
	console.log("Populating skills and industries.");
	const skillArr =[];
	const industryArr = [];
	skillStr
		.split("|")
		.forEach(function(skill) { 
			if(skill) {
				skillArr.push({name: skill});
			}
		});
	industryStr
		.split("|")
		.forEach(function(industry) {
			if(industry) {
				industryArr.push({name: industry});
			}
		});
	await Skill.insertMany(skillArr);
	await Industry.insertMany(industryArr);
}

var setupData = async function() {
	console.log("Populating skills and industries meta info.")
	if(await checkIfSkillsPopulated()) {
		console.log("Data already populated. Exiting data setup.")
		return;
	}
	try {
		await populateSkillsAndIndustryMeta();
	} catch (err) {
		console.log(err);
	}
}

module.exports = {
	setupData : setupData
}