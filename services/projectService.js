const lodash = require('lodash');
const Applicant = require('../models/Applicant');
const Project = require('../models/Project')
require('../models/Skill')
require('../models/Industry')


var create = async function(projectDetails) {
	const project = new Project(projectDetails);
	const savedProject = await project.save()
	return await findOne(savedProject._id);
}

var findOne = function(projectId) {
	return Project.findOne({
		_id: projectId
	}).populate('skills industry');
}

var list = function() {
	return Project
    .find()
    .populate('skills industry');
}

module.exports = {
  create: create,
  list: list,
  findOne: findOne,
}