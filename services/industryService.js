const Industry = require('../models/Industry')

var searchIndustryByName = function(name = "") {
	return Industry.find({
		name: {
			'$regex': name,
			'$options': 'i'
		}
	});
}

module.exports = {
	searchIndustryByName: searchIndustryByName
}