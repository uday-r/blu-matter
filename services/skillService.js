const Skill = require('../models/Skill')

var searchSkillByName = function(name = "") {
	return Skill.find({
		name: {
			'$regex': name,
			'$options': 'i'
		}
	});
}

module.exports = {
	searchSkillByName: searchSkillByName
}