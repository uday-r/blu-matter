const lodash = require('lodash');
const Applicant = require('../models/Applicant');
const Project = require('../models/Project')
require('../models/Skill')
require('../models/Industry')


var getMatchingApplicantsByPercentage = function(projectDetails) {

  let skillsExp = projectDetails.skills.reduce(function(skillStr, it) {
    skillStr.push(it.name);
    return skillStr;
  }, []).join('|');

  let industryName = projectDetails.industry.name;
  let basicQuery = [
    {
      $unwind: '$skills'
    },
    {
      $lookup: {
        from: 'skills',
        localField: 'skills',
        foreignField: '_id',
        as: "skills"
      }
    },
    {
      $lookup: {
        from: 'industry',
        localField: 'industry',
        foreignField: '_id',
        as: "industry"
      }
    },
  ];

  basicQuery.push({
    $match:  { 
        skills:  {
          $elemMatch: {
          name: { $regex: skillsExp, $options: 'i'}
        }
      }
    }
  })
  basicQuery.push({
    $match:  { 
      industry:  {
        $elemMatch: {
          name: { $regex: industryName, $options: 'i' }
        }
      }
    }
  })

  //Adding skills count.
  basicQuery.push({
    $group: {
      _id: {
        _id: "$_id",
        skills: {$arrayElemAt: [ "$skills", 0 ]},
        name: "$name",
        industry: "$industry",
        experience: "$experience",
        cv: "$cv"
      },
      skillsCount: {
        $sum: 1
      }
    }
  });

  //Group skills into a particular user
  basicQuery.push({
    $group: {
      _id: {
        _id: "$_id._id",
        name: "$_id.name",
        industry: "$_id.industry",
        experience: "$_id.experience",
        cv: "$_id.cv"
      },
      skills: {
        $push: {
          id: "$$CURRENT._id.skills._id",
          name: "$$CURRENT._id.skills.name",
          count: "$skillsCount"
        }
      }
    }
  });

  //Project user information properly
  basicQuery.push({
    $project: {
      _id: "$$CURRENT._id._id",
      name: "$$CURRENT._id.name",
      skills: "$$CURRENT.skills",
      industry: "$$CURRENT._id.industry",
      experience: "$$CURRENT._id.experience",
      cv: "$$CURRENT._id.cv"
    }
  });

  //Flatten industry object as its always a single element.
  basicQuery.push({
    $unwind: "$industry"
  })
  
  console.log("Running query: ", JSON.stringify(basicQuery));

  return new Promise(function(resolve, reject) {
    Applicant
    .aggregate(basicQuery)
    .then(function(applicantRows) {
      applicantRows.forEach(function(row) {
        if(!row.skills) {
          return;
        }
        row.matchPercentage = (row.skills.length/projectDetails.skills.length) * 100;
      });
      const sortedApplicants = lodash.orderBy(applicantRows, 'matchPercentage', 'desc');
      resolve(sortedApplicants);
    })
    .catch(function(err) {
      console.log('ERR', err);
      reject(err)
    })
  });
}

var getApplicantsByCVMatch = function(skills, industryName) {
  let skillsReg = skills.join("|");
  const query = [
    {
      $project: {
        _id: "$$CURRENT._id",
        name: "$$CURRENT.name",
        skills: "$$CURRENT.skills",
        industry: "$$CURRENT.industry",
        experience: "$$CURRENT.experience",
        cv: "$$CURRENT.cv",
        words: { $split: ["$cv", " "] }
      }
    },
    {
      $lookup: {
        from: 'industry',
        localField: 'industry',
        foreignField: '_id',
        as: "industry"
      }
    },
    {
      $lookup: {
        from: 'skills',
        localField: 'skills',
        foreignField: '_id',
        as: "skills"
      }
    },
    {
      $unwind: '$words'
    },
    {
      $match: {
        words: { $regex: "dev", $options: 'gi'}
      }
    },
    {
      $match:  { 
        industry:  {
          $elemMatch: {
            name: { $regex: industryName, $options: 'i' }
          }
        }
      }
    },
    {
      $group: {
        _id: {
          _id: "$$CURRENT._id",
          name: "$$CURRENT.name",
          skills: "$$CURRENT.skills",
          industry: "$$CURRENT.industry",
          experience: "$$CURRENT.experience",
          cv: "$$CURRENT.cv",
         },
        allCvMatchs: { $push: "$words"},
        cvMatchCount: { $sum: 1}
      }
    },
    {
      $sort:{cvMatchCount:-1}
    },
    {
      $project: {
        _id: "$$CURRENT._id._id",
        name: "$$CURRENT._id.name",
        skills: "$$CURRENT._id.skills",
        industry: "$$CURRENT._id.industry",
        experience: "$$CURRENT._id.experience",
        cv: "$$CURRENT._id.cv",
        allCVMatches: "$$CURRENT.allCvMatchs"
      }
    } 
  ];
  console.log("Ruinning query: ", query);
  return Applicant
    .aggregate(query);
}

var getApplicantProirityListByProjectId = async function(projectId) {
  const projectDetails = await Project.findById(projectId).populate('skills industry');
  
  console.log(projectDetails);

  const applicantsWithPercentage = await getMatchingApplicantsByPercentage(projectDetails);
  const priorityList = await getApplicantsByCVMatch(projectDetails.skills, projectDetails.industry.name);

  let resultSet = [];


  priorityList.forEach(function(priorityApplicant) {
    const matchingApplicantId = lodash.findIndex(applicantsWithPercentage, function(it) {
      return priorityApplicant._id.equals(it._id);
    });
    if(matchingApplicantId == -1) {
      resultSet.push(priorityApplicant);
      return;
    }
    const matchedApplicantWithPercentage = applicantsWithPercentage[matchingApplicantId];
    applicantsWithPercentage.splice(matchingApplicantId, 1);
    matchedApplicantWithPercentage.allCVMatches = priorityApplicant.allCVMatches;
    resultSet.push(matchedApplicantWithPercentage);
  });

  resultSet = lodash.concat(resultSet, applicantsWithPercentage);

  resultSet.forEach(function(result) {
    result.priority = result.experience + (result.matchPercentage/100) + (result.allCVMatches? result.allCVMatches.length: 0);
  })

  return resultSet;
}

var list = function() {
  return Applicant
    .find({})
    .populate('industry skills');
}

var create = async function(details) {
  const applicant = new Applicant(details);
  const savedApplicant = await applicant.save()
  return await findOne(savedApplicant._id);
}

var findOne = function(applicantId) {
  return Applicant.findOne({
    _id: applicantId
  }).populate('skills industry');
}

module.exports = {
  getApplicantProirityListByProjectId: getApplicantProirityListByProjectId,
  list: list,
  create: create,
}