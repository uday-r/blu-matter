var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var projectRoutes = require('./routes/projects')
var applicantRoutes = require('./routes/applicants')
var skillRoutes = require('./routes/skills')
var industryRoutes = require('./routes/industries')

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use("/api/projects", projectRoutes);
app.use("/api/applicants", applicantRoutes);
app.use("/api/industries", industryRoutes);
app.use("/api/skills", skillRoutes);

var utils = require("./utils");
utils.setupData();

module.exports = app;
