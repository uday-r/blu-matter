const mongoose   = require('mongoose');

const skillSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        required: true,
    }
}, { collection: 'skills' })
skillSchema.set('toJSON', {
  transform : function (doc, ret, options) {
  // remove the _id of every document before returning the result
  ret.id = ret._id;
  delete ret._id;
  delete ret.__v;
  }
});
module.exports = mongoose.model('Skill', skillSchema)