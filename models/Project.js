const mongoose   = require('mongoose');
var Schema = mongoose.Schema;

const projectSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true
  },
  industry: {
    type: Schema.Types.ObjectId, ref: 'Industry'
  },
  skills: [
    {
      type: Schema.Types.ObjectId, ref: 'Skill'
    }
  ],
  reqExperience: {
    type: Number,
    required: true
  }
}, { collection: 'project' })
projectSchema.set('toJSON', {
  transform : function (doc, ret, options) {
  // remove the _id of every document before returning the result
  ret.id = ret._id;
  delete ret._id;
  delete ret.__v;
  }
});
module.exports = mongoose.model('Project', projectSchema)